/*
 * ZrtpInterface.cc
 *
 *  Created on: Jan 7, 2019
 *      Author: mstarosz
 */

#include "ZrtpInterface.h"

namespace inet {

namespace rtp {

ZrtpInterface::ZrtpInterface() {
    // TODO Auto-generated constructor stub

}

ZrtpInterface::~ZrtpInterface() {
    // TODO Auto-generated destructor stub
}

void ZrtpInterface::handleMessage(cMessage *msgIn) {
    RtpInnerPacket *pk = check_and_cast<RtpInnerPacket *>(msgIn);
    Packet *packet = check_and_cast<Packet *>(pk->getEncapsulatedPacket());
    const auto& zrtpHeader = packet->peekAtFront<ZrtpHeader>();

    zrtpHeader->logInfo();

    switch (currentState) {
        case State::INACTIVE: {
            throw cRuntimeError("Host is not ready to handle message.");
            break;
        } case State::W8_FOR_HELLO: {
            if (zrtpHeader->getType() != HELLO) {
                throw cRuntimeError("Invalid message type. Expecting Hello message.");
            }
            break;
        } case State::W8_FOR_HELLO_ACK: {
            if (zrtpHeader->getType() != HELLO_ACK) {
                throw cRuntimeError("Invalid message type. Expecting HelloACK message.");
            }
            break;
        } case State::W8_FOR_DH_PART_1: {
            if (zrtpHeader->getType() != DH_PART_1) {
                throw cRuntimeError("Invalid message type. Expecting DHPart1 message.");
            }
            break;
        } case State::W8_FOR_DH_PART_2: {
            if (zrtpHeader->getType() != DH_PART_2) {
                throw cRuntimeError("Invalid message type. Expecting DHPart2 message.");
            }
            break;
        } case State::W8_FOR_COMMIT: {
            if (zrtpHeader->getType() != COMMIT) {
                throw cRuntimeError("Invalid message type. Expecting Commit message.");
            }
            break;
        } case State::W8_FOR_CONFIRM_1: {
            if (zrtpHeader->getType() != CONFIRM_1) {
                throw cRuntimeError("Invalid message type. Expecting HelloACK message.");
            }
            break;
        } case State::W8_FOR_CONFIRM_2: {
            if (zrtpHeader->getType() != CONFIRM_2) {
                throw cRuntimeError("Invalid message type. Expecting Confirm2 message.");
            }
            break;
        } case State::W8_FOR_CONF_2_ACK: {
            if (zrtpHeader->getType() != CONF_2_ACK) {
                throw cRuntimeError("Invalid message type. Expecting Conf2ACK message.");
            }
            break;
        } default: {
            break;
        }
    }
}

RtpInnerPacket* ZrtpInterface::prepareHello() {
    return nullptr;
}

RtpInnerPacket* ZrtpInterface::prepareHelloAck() {
    return nullptr;
}

RtpInnerPacket* ZrtpInterface::prepareCommit() {
    return nullptr;
}

RtpInnerPacket* ZrtpInterface::prepareDHPart1() {
    return nullptr;
}

RtpInnerPacket* ZrtpInterface::prepareDHPart2() {
    return nullptr;
}

RtpInnerPacket* ZrtpInterface::prepareConfirm1() {
    return nullptr;
}

RtpInnerPacket* ZrtpInterface::prepareConfirm2() {
    return nullptr;
}

RtpInnerPacket* ZrtpInterface::prepareConf2Ack() {
    return nullptr;
}

void ZrtpInterface::setCurrentState(State state) {
    currentState = state;
}

State ZrtpInterface::getCurrentState() {
    return currentState;
}

}
}

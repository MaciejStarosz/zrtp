//
// Copyright (C) 2001 Matthias Oppitz <Matthias.Oppitz@gmx.de>
// Copyright (C) 2007 Ahmed Ayadi  <ahmed.ayadi@sophia.inria.fr>
// Copyright (C) 2010 Zoltan Bojthe
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

#include "../zrtpapp/ZrtpApplication.h"

namespace inet {

using namespace rtp;

Define_Module(ZrtpApplication)

void ZrtpApplication::sendHello() {
    EV_INFO << "SEND Zrtp Hello Packet" << endl;
    Packet *packet = new Packet("Zrtp Hello");
    const auto& zrtpHeader = makeShared<ZrtpHello>();
    zrtpHeader->setChunkLength(B(zrtpHeader->getLength()));
    packet->insertAtFront(zrtpHeader);
    RtpInnerPacket *rinpOut = new RtpInnerPacket("dataOut()");
    rinpOut->setDataOutPkt(packet);
    send(rinpOut, "zrtpOut");

}

void ZrtpApplication::sendHelloAck() {
    EV_INFO << "SEND Zrtp HelloAck Packet" << endl;
    Packet *packet = new Packet("Zrtp HelloAck");
    const auto& zrtpHeader = makeShared<ZrtpHelloAck>();
    zrtpHeader->setChunkLength(B(zrtpHeader->getLength()));
    packet->insertAtFront(zrtpHeader);
    RtpInnerPacket *rinpOut = new RtpInnerPacket("dataOut()");
    rinpOut->setDataOutPkt(packet);
    send(rinpOut, "zrtpOut");
}

void ZrtpApplication::sendCommit() {
    EV_INFO << "SEND Zrtp Commit Packet" << endl;
    Packet *packet = new Packet("Zrtp Commit");
    const auto& zrtpHeader = makeShared<ZrtpCommit>();
    zrtpHeader->setChunkLength(B(zrtpHeader->getLength()));
    packet->insertAtFront(zrtpHeader);
    RtpInnerPacket *rinpOut = new RtpInnerPacket("dataOut()");
    rinpOut->setDataOutPkt(packet);
    send(rinpOut, "zrtpOut");
}

void ZrtpApplication::sendDHPart1() {
    EV_INFO << "SEND Zrtp DHPart1 Packet" << endl;
    Packet *packet = new Packet("Zrtp DHPart1");
    const auto& zrtpHeader = makeShared<ZrtpDHPart1>();
    zrtpHeader->setChunkLength(B(zrtpHeader->getLength()));
    packet->insertAtFront(zrtpHeader);
    RtpInnerPacket *rinpOut = new RtpInnerPacket("dataOut()");
    rinpOut->setDataOutPkt(packet);
    send(rinpOut, "zrtpOut");
}

void ZrtpApplication::sendDHPart2() {
    EV_INFO << "SEND Zrtp DHPart2 Packet" << endl;
    Packet *packet = new Packet("Zrtp DHPart2");
    const auto& zrtpHeader = makeShared<ZrtpDHPart2>();
    zrtpHeader->setChunkLength(B(zrtpHeader->getLength()));
    packet->insertAtFront(zrtpHeader);
    RtpInnerPacket *rinpOut = new RtpInnerPacket("dataOut()");
    rinpOut->setDataOutPkt(packet);
    send(rinpOut, "zrtpOut");
}

void ZrtpApplication::sendConfirm1() {
    EV_INFO << "SEND Zrtp Confirm1 Packet" << endl;
    Packet *packet = new Packet("Zrtp Confirm1");
    const auto& zrtpHeader = makeShared<ZrtpConfirm1>();
    zrtpHeader->setChunkLength(B(zrtpHeader->getLength()));
    packet->insertAtFront(zrtpHeader);
    RtpInnerPacket *rinpOut = new RtpInnerPacket("dataOut()");
    rinpOut->setDataOutPkt(packet);
    send(rinpOut, "zrtpOut");
}

void ZrtpApplication::sendConfirm2() {
    EV_INFO << "SEND Zrtp Confirm2 Packet" << endl;
    Packet *packet = new Packet("Zrtp Confirm2");
    const auto& zrtpHeader = makeShared<ZrtpConfirm2>();
    zrtpHeader->setChunkLength(B(zrtpHeader->getLength()));
    packet->insertAtFront(zrtpHeader);
    RtpInnerPacket *rinpOut = new RtpInnerPacket("dataOut()");
    rinpOut->setDataOutPkt(packet);
    send(rinpOut, "zrtpOut");
}

void ZrtpApplication::sendConf2Ack() {
    EV_INFO << "SEND Zrtp Conf2Ack Packet" << endl;
    Packet *packet = new Packet("Zrtp Conf2Ack");
    const auto& zrtpHeader = makeShared<ZrtpConf2Ack>();
    zrtpHeader->setChunkLength(B(zrtpHeader->getLength()));
    packet->insertAtFront(zrtpHeader);
    RtpInnerPacket *rinpOut = new RtpInnerPacket("dataOut()");
    rinpOut->setDataOutPkt(packet);
    send(rinpOut, "zrtpOut");
}

void ZrtpApplication::handleMessageAsSender(const inet::IntrusivePtr<const inet::rtp::ZrtpHeader>& zrtpHeader) {
    zrtpHeader->logInfo();
    switch (zrtpHeader->getType()) {
        case HELLO: {
            EV_INFO << "HELLO MSG received!" << endl;
            sendHelloAck();
            break;
        } case HELLO_ACK: {
            EV_INFO << "HELLOACK MSG received!" << endl;
            break;
        } case COMMIT: {
            EV_INFO << "COMMIT MSG received!" << endl;
            sendDHPart1();
            break;
        } case DH_PART_1: {
            EV_INFO << "DHPART2 MSG received!" << endl;
            break;
        } case DH_PART_2: {
            EV_INFO << "DHPART2 MSG received!" << endl;
            sendConfirm1();
            break;
        } case CONFIRM_1: {
            EV_INFO << "CONFIRM1 MSG received!" << endl;
            break;
        } case CONFIRM_2: {
            EV_INFO << "CONFIRM2 MSG received!" << endl;
            sendConf2Ack();
            cMessage *selfMsg = new cMessage("startTransmission", START_TRANSMISSION);
            scheduleAt(simTime() + transmissionStartDelay, selfMsg);
            break;
        } case CONF_2_ACK: {
            EV_INFO << "CONF2ACK MSG received!" << endl;
            break;
        } default: {
            break;
        }

    }
}

void ZrtpApplication::handleMessageAsReciver(const inet::IntrusivePtr<const inet::rtp::ZrtpHeader>& zrtpHeader) {
    zrtpHeader->logInfo();
    switch (zrtpHeader->getType()) {
        case HELLO: {
            EV_INFO << "HELLO MSG received!" << endl;
            sendHelloAck();
            sendHello();
            break;
        } case HELLO_ACK: {
            EV_INFO << "HELLOACK MSG received!" << endl;
            sendCommit();
            break;
        } case COMMIT: {
            EV_INFO << "COMMIT MSG received!" << endl;
            break;
        } case DH_PART_1: {
            EV_INFO << "DHPART2 MSG received!" << endl;
            sendDHPart2();
            break;
        } case DH_PART_2: {
            EV_INFO << "DHPART2 MSG received!" << endl;
            break;
        } case CONFIRM_1: {
            EV_INFO << "CONFIRM1 MSG received!" << endl;
            sendConfirm2();
            break;
        } case CONFIRM_2: {
            EV_INFO << "CONFIRM2 MSG received!" << endl;
            break;
        } case CONF_2_ACK: {
            EV_INFO << "CONF2ACK MSG received!" << endl;
            cMessage *selfMsg = new cMessage("startTransmission", START_TRANSMISSION);
            scheduleAt(simTime() + transmissionStartDelay, selfMsg);
            break;
        } default: {
            break;
        }

    }
}


void ZrtpApplication::initialize(int stage)
{
    cSimpleModule::initialize(stage);

    // because of L3AddressResolver, we need to wait until interfaces are registered,
    // address auto-assignment takes place etc.
    if (stage == INITSTAGE_LOCAL) {
        // the common name (CNAME) of this host
        commonName = par("commonName");

        // which rtp profile is to be used (usually RtpAvProfile)
        profileName = par("profileName");

        // bandwidth in bytes per second for this session
        bandwidth = par("bandwidth");

        // port number which is to be used; to ports are actually used: one
        // for rtp and one for rtcp
        port = par("portNumber");

        // fileName of file to be transmitted
        // nullptr or "" means this system acts only as a receiver
        fileName = par("fileName");

        // payload type of file to transmit
        payloadType = par("payloadType");

        sessionEnterDelay = par("sessionEnterDelay");
        transmissionStartDelay = par("transmissionStartDelay");
        transmissionStopDelay = par("transmissionStopDelay");
        sessionLeaveDelay = par("sessionLeaveDelay");

        ssrc = 0;
        isActiveSession = false;
        isSender = par("isSender");

        if (isSender) {
            zrtpInterface = new ZrtpSender();
            zrtpInterface->setCurrentState(State::W8_FOR_HELLO_ACK);
        } else {
            zrtpInterface = new ZrtpReceiver();
            zrtpInterface->setCurrentState(State::W8_FOR_HELLO);
        }

    }
    else if (stage == INITSTAGE_APPLICATION_LAYER) {
        cModule *node = findContainingNode(this);
        NodeStatus *nodeStatus = node ? check_and_cast_nullable<NodeStatus *>(node->getSubmodule("status")) : nullptr;
        bool isOperational = (!nodeStatus) || nodeStatus->getState() == NodeStatus::UP;
        if (!isOperational)
            throw cRuntimeError("This module doesn't support starting in node DOWN state");

        // the ip address to connect to (unicast or multicast)
        destinationAddress = L3AddressResolver().resolve(par("destinationAddress")).toIpv4();

        EV_DETAIL << "commonName" << commonName << endl
                  << "profileName" << profileName << endl
                  << "bandwidth" << bandwidth << endl
                  << "destinationAddress" << destinationAddress << endl
                  << "portNumber" << port << endl
                  << "fileName" << fileName << endl
                  << "payloadType" << payloadType << endl;


        cMessage *selfMsg = new cMessage("enterSession", ENTER_SESSION);
        scheduleAt(simTime() + sessionEnterDelay, selfMsg);
    }
}

void ZrtpApplication::handleMessage(cMessage *msgIn) {
    using namespace rtp;

    if (msgIn->isSelfMessage()) {
        switch (msgIn->getKind()) {
            case START_ZRTP: {
                EV_INFO << "start ZRTP" << endl;
                RtpInnerPacket *zrtpHelloMsg = zrtpInterface->prepareHello();
                send(zrtpHelloMsg, "zrtpOut");
                break;
            }
            case LISTEN_ZRTP: {
                EV_INFO << "listen ZRTP" << endl;
                break;
            }
            case ENTER_SESSION: {
                EV_INFO << "enterSession" << endl;
                if (isActiveSession) {
                    EV_WARN << "Session already entered\n";
                }
                else {
                    isActiveSession = true;
                    RtpCiEnterSession *ci = new RtpCiEnterSession();
                    ci->setCommonName(commonName);
                    ci->setProfileName(profileName);
                    ci->setBandwidth(bandwidth);
                    ci->setDestinationAddress(destinationAddress);
                    ci->setPort(port);
                    cMessage *msg = new RtpControlMsg("Enter Session");
                    msg->setControlInfo(ci);
                    send(msg, "rtpOut");
                }
                break;
            }
            case START_TRANSMISSION: {
                EV_INFO << "startTransmission" << endl;
                if (!isActiveSession) {
                    EV_WARN << "Session already left\n";
                }
                else {
                    RtpCiSenderControl *ci = new RtpCiSenderControl();
                    ci->setCommand(RTP_CONTROL_PLAY);
                    ci->setSsrc(ssrc);
                    cMessage *msg = new RtpControlMsg("senderModuleControl(PLAY)");
                    msg->setControlInfo(ci);
                    send(msg, "rtpOut");

                    cMessage *selfMsg = new cMessage("stopTransmission", STOP_TRANSMISSION);
                    scheduleAt(simTime() + transmissionStopDelay, selfMsg);
                }
                break;
            }
            case STOP_TRANSMISSION: {
                EV_INFO << "stopTransmission" << endl;
                if (!isActiveSession) {
                    EV_WARN << "Session already left\n";
                }
                else {
                    RtpCiSenderControl *ci = new RtpCiSenderControl();
                    ci->setCommand(RTP_CONTROL_STOP);
                    ci->setSsrc(ssrc);
                    cMessage *msg = new RtpControlMsg("senderModuleControl(STOP)");
                    msg->setControlInfo(ci);
                    send(msg, "rtpOut");
                }
                break;
            }
            case LEAVE_SESSION: {
                EV_INFO << "leaveSession" << endl;
                if (!isActiveSession) {
                    EV_WARN << "Session already left\n";
                }
                else {
                    RtpCiLeaveSession *ci = new RtpCiLeaveSession();
                    cMessage *msg = new RtpControlMsg("Leave Session");
                    msg->setControlInfo(ci);
                    send(msg, "rtpOut");
                }
                break;
            }
            default:{
                throw cRuntimeError("Invalid msgKind value %d in message '%s'",
                    msgIn->getKind(), msgIn->getName());
            }
        }
    }
    else if (isActiveSession) {
        cObject *obj = msgIn->removeControlInfo();
        RtpControlInfo *ci = dynamic_cast<RtpControlInfo *>(obj);
        if (ci) {
            switch (ci->getType()) {
                case RTP_IFP_SESSION_ENTERED: {
                    EV_INFO << "Session Entered" << endl;
                    ssrc = (check_and_cast<RtpCiSessionEntered *>(ci))->getSsrc();
                    if (opp_strcmp(fileName, "")) {
                        EV_INFO << "CreateSenderModule" << endl;
                        RtpCiCreateSenderModule *ci = new RtpCiCreateSenderModule();
                        ci->setSsrc(ssrc);
                        ci->setPayloadType(payloadType);
                        ci->setFileName(fileName);
                        cMessage *msg = new RtpControlMsg("createSenderModule()");
                        msg->setControlInfo(ci);
                        send(msg, "rtpOut");
                    } else {
                        cMessage *selfMsg = new cMessage("leaveSession", LEAVE_SESSION);
                        EV_INFO << "Receiver Module : leaveSession" << endl;
                        scheduleAt(simTime() + sessionLeaveDelay, selfMsg);
                    }
                    break;
                }
                case RTP_IFP_SENDER_MODULE_CREATED: {
                    EV_INFO << "Sender Module Created" << endl;
                    if (isSender) {
                        cMessage *selfMsg = new cMessage("startZrtp", START_ZRTP);
                        scheduleAt(simTime() + sessionEnterDelay, selfMsg);
                    } else {
                        cMessage *selfMsg = new cMessage("listenZrtp", LISTEN_ZRTP);
                        scheduleAt(simTime() + sessionEnterDelay, selfMsg);
                    }
                    break;
                }
                case RTP_IFP_SENDER_STATUS: {
                    cMessage *selfMsg;
                    RtpCiSenderStatus *rsim = check_and_cast<RtpCiSenderStatus *>(ci);
                    switch (rsim->getStatus()) {
                        case RTP_SENDER_STATUS_PLAYING: {
                            EV_INFO << "PLAYING" << endl;
                            break;
                        }
                        case RTP_SENDER_STATUS_FINISHED: {
                            EV_INFO << "FINISHED" << endl;
                            selfMsg = new cMessage("leaveSession", LEAVE_SESSION);
                            scheduleAt(simTime() + sessionLeaveDelay, selfMsg);
                            break;
                        }
                        case RTP_SENDER_STATUS_STOPPED: {
                            EV_INFO << "STOPPED" << endl;
                            selfMsg = new cMessage("leaveSession", LEAVE_SESSION);
                            scheduleAt(simTime() + sessionLeaveDelay, selfMsg);
                            break;
                        }
                        default: {
                            throw cRuntimeError("Invalid sender status: %d", rsim->getStatus());
                        }
                    }
                    break;
                }
                case RTP_IFP_SESSION_LEFT: {
                    if (!isActiveSession) {
                        EV_WARN << "Session already left\n";
                    } else {
                        isActiveSession = false;
                    }
                    break;
                }
                case RTP_IFP_SENDER_MODULE_DELETED: {
                    EV_INFO << "Sender Module Deleted" << endl;
                    break;
                }
                default: {
                    break;
                }
            }
        } else {
            zrtpInterface->handleMessage(msgIn);

            while (!zrtpInterface->msgQueue->isEmpty()) {
                RtpInnerPacket *rtpInnerPacket = check_and_cast<RtpInnerPacket *>(zrtpInterface->msgQueue->pop());
                send(rtpInnerPacket, "zrtpOut");
            }
        }

        delete obj;
    }

    delete msgIn;
}



} // namespace inet


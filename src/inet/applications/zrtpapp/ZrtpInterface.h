/*
 * ZrtpInterface.h
 *
 *  Created on: Jan 7, 2019
 *      Author: mstarosz
 */

#include "inet/transportlayer/rtp/RtpInnerPacket_m.h"
#include "inet/transportlayer/rtp/zrtp/ZrtpHeader_m.h"
#include <stdlib.h>
#include <time.h>
#include <climits>

#ifndef INET_APPLICATIONS_ZRTPAPP_ZRTPINTERFACE_H_
#define INET_APPLICATIONS_ZRTPAPP_ZRTPINTERFACE_H_

namespace inet {

namespace rtp {

enum class State {
    INACTIVE,
    W8_FOR_HELLO,
    W8_FOR_HELLO_ACK,
    W8_FOR_DH_PART_1,
    W8_FOR_DH_PART_2,
    W8_FOR_COMMIT,
    W8_FOR_CONFIRM_1,
    W8_FOR_CONFIRM_2,
    W8_FOR_CONF_2_ACK,
    SESSION_ESTABLISHED
};

class ZrtpInterface {
public:
    ZrtpInterface();
    virtual ~ZrtpInterface();

    virtual void handleMessage(cMessage *msgIn);

    virtual RtpInnerPacket* prepareHello();

    virtual RtpInnerPacket* prepareHelloAck();

    virtual RtpInnerPacket* prepareCommit();

    virtual RtpInnerPacket* prepareDHPart1();

    virtual RtpInnerPacket* prepareDHPart2();

    virtual RtpInnerPacket* prepareConfirm1();

    virtual RtpInnerPacket* prepareConfirm2();

    virtual RtpInnerPacket* prepareConf2Ack();

    virtual void setCurrentState(State state);

    virtual State getCurrentState();

    cQueue * msgQueue = new cQueue("MessageQueue", nullptr);

    uint16 sequenceNumber;

protected:

    State currentState = State::INACTIVE;


};

}

}
#endif /* INET_APPLICATIONS_ZRTPAPP_ZRTPINTERFACE_H_ */

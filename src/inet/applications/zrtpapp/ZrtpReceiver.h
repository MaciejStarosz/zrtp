/*
 * ZrtpReceiver.h
 *
 *  Created on: Jan 7, 2019
 *      Author: mstarosz
 */

#include "ZrtpInterface.h"
#include "inet/transportlayer/rtp/zrtp/ZrtpHeader_m.h"

#ifndef INET_APPLICATIONS_ZRTPAPP_ZRTPRECEIVER_H_
#define INET_APPLICATIONS_ZRTPAPP_ZRTPRECEIVER_H_

namespace inet {

namespace rtp {

class ZrtpReceiver : public ZrtpInterface {
public:
    ZrtpReceiver();
    virtual ~ZrtpReceiver();

    virtual void handleMessage(cMessage *msgIn) override;

    virtual RtpInnerPacket* prepareHello() override;

    virtual RtpInnerPacket* prepareHelloAck() override;

    virtual RtpInnerPacket* prepareCommit() override;

    virtual RtpInnerPacket* prepareDHPart2() override;

    virtual RtpInnerPacket* prepareConfirm2() override;

};

}

}
#endif /* INET_APPLICATIONS_ZRTPAPP_ZRTPRECEIVER_H_ */

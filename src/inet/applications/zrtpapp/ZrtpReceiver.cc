/*
 * ZrtpReceiver.cc
 *
 *  Created on: Jan 7, 2019
 *      Author: mstarosz
 */

#include "ZrtpReceiver.h"

namespace inet {

namespace rtp {

ZrtpReceiver::ZrtpReceiver() {
    // TODO Auto-generated constructor stub

}

ZrtpReceiver::~ZrtpReceiver() {
    // TODO Auto-generated destructor stub
}

void ZrtpReceiver::handleMessage(cMessage *msgIn) {
    ZrtpInterface::handleMessage(msgIn);
    switch (currentState) {
        case State::W8_FOR_HELLO: {
            msgQueue->insert(prepareHelloAck());
            msgQueue->insert(prepareHello());
            currentState = State::W8_FOR_HELLO_ACK;
            break;
        } case State::W8_FOR_HELLO_ACK: {
            msgQueue->insert(prepareCommit());
            currentState = State::W8_FOR_DH_PART_1;
            break;
        } case State::W8_FOR_DH_PART_1: {
            msgQueue->insert(prepareDHPart2());
            currentState = State::W8_FOR_CONFIRM_1;
            break;
        } case State::W8_FOR_CONFIRM_1: {
            msgQueue->insert(prepareConfirm2());
            currentState = State::W8_FOR_CONF_2_ACK;
            break;
        } case State::W8_FOR_CONF_2_ACK: {
            EV_INFO << "CONF2ACK MSG received!" << endl;
            currentState = State::SESSION_ESTABLISHED;
            //cMessage *selfMsg = new cMessage("startTransmission", START_TRANSMISSION);
            //scheduleAt(simTime() + transmissionStartDelay, selfMsg);
            break;
        } default: {
            break;
        }

    }
}

RtpInnerPacket* ZrtpReceiver::prepareHello() {
    EV_INFO << "Preparing ZRTP Hello Packet" << endl;
    Packet *packet = new Packet("Hello");
    const auto& zrtpHello = makeShared<ZrtpHello>();

    srand (time(NULL));
    sequenceNumber = rand() % SHRT_MAX + 1;
    zrtpHello->setSequenceNumber(sequenceNumber);
    zrtpHello->setChunkLength(B(zrtpHello->getLength()));
    zrtpHello->setHashTypeCount(1);
    zrtpHello->setHashAlghoritmsArraySize(1);
    zrtpHello->setHashAlghoritms(0, HASH_TYPE_BLOCK::S256);
    zrtpHello->setCipherTypeCount(1);
    zrtpHello->setCipherAlghoritmsArraySize(1);
    zrtpHello->setCipherAlghoritms(0, CIPHER_TYPE_BLOCK::AES2);
    zrtpHello->setAuthTypeCount(1);
    zrtpHello->setAuthTagTypesArraySize(1);
    zrtpHello->setAuthTagTypes(0, AUTH_TAG_TYPE_BLOCK::HS32);
    zrtpHello->setKeyAgreementTypeCount(1);
    zrtpHello->setKeyAgreementTypesArraySize(1);
    zrtpHello->setKeyAgreementTypes(0, KEY_AGREEMENT_TYPE_BLOCK::DH3K);
    zrtpHello->setSasTypeCount(2);
    zrtpHello->setSasTypesArraySize(2);
    zrtpHello->setSasTypes(0, SAS_TYPE_BLOCK::B32);
    zrtpHello->setSasTypes(1, SAS_TYPE_BLOCK::B256);

    zrtpHello->setChunkLength(B(zrtpHello->getLength()));
    packet->insertAtFront(zrtpHello);
    RtpInnerPacket *rtpInnerPacket = new RtpInnerPacket();
    rtpInnerPacket->setDataOutPkt(packet);
    return rtpInnerPacket;
}

RtpInnerPacket* ZrtpReceiver::prepareHelloAck() {
    EV_INFO << "Preparing ZRTP HelloACK Packet" << endl;
    Packet *packet = new Packet("Commit");
    const auto& zrtpHelloAck = makeShared<ZrtpHelloAck>();
    zrtpHelloAck->setChunkLength(B(zrtpHelloAck->getLength()));

    sequenceNumber = sequenceNumber + 1;
    zrtpHelloAck->setSequenceNumber(sequenceNumber);

    packet->insertAtFront(zrtpHelloAck);
    RtpInnerPacket *rtpInnerPacket = new RtpInnerPacket();
    rtpInnerPacket->setDataOutPkt(packet);
    return rtpInnerPacket;
}

RtpInnerPacket* ZrtpReceiver::prepareCommit() {
    EV_INFO << "Preparing ZRTP Commit Packet" << endl;
    Packet *packet = new Packet("Commit");
    const auto& zrtpCommit = makeShared<ZrtpCommit>();
    zrtpCommit->setChunkLength(B(zrtpCommit->getLength()));

    sequenceNumber = sequenceNumber + 1;
    zrtpCommit->setSequenceNumber(sequenceNumber);

    packet->insertAtFront(zrtpCommit);
    RtpInnerPacket *rtpInnerPacket = new RtpInnerPacket();
    rtpInnerPacket->setDataOutPkt(packet);
    return rtpInnerPacket;
}

RtpInnerPacket* ZrtpReceiver::prepareDHPart2() {
    EV_INFO << "Preparing ZRTP DHPart2 Packet" << endl;
    Packet *packet = new Packet("DHPart2");
    const auto& zrtpDHPart2 = makeShared<ZrtpDHPart2>();
    zrtpDHPart2->setChunkLength(B(zrtpDHPart2->getLength()));
    packet->insertAtFront(zrtpDHPart2);

    sequenceNumber = sequenceNumber + 1;
    zrtpDHPart2->setSequenceNumber(sequenceNumber);

    RtpInnerPacket *rtpInnerPacket = new RtpInnerPacket();
    rtpInnerPacket->setDataOutPkt(packet);
    return rtpInnerPacket;
}

RtpInnerPacket* ZrtpReceiver::prepareConfirm2() {
    EV_INFO << "Preparing ZRTP Confirm2 Packet" << endl;
    Packet *packet = new Packet("Confirm2");
    const auto& zrtpConfirm2 = makeShared<ZrtpConfirm2>();
    zrtpConfirm2->setChunkLength(B(zrtpConfirm2->getLength()));

    sequenceNumber = sequenceNumber + 1;
    zrtpConfirm2->setSequenceNumber(sequenceNumber);

    packet->insertAtFront(zrtpConfirm2);
    RtpInnerPacket *rtpInnerPacket = new RtpInnerPacket();
    rtpInnerPacket->setDataOutPkt(packet);
    return rtpInnerPacket;
}

}

}

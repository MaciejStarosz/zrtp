/*
 * ZrtpSender.h
 *
 *  Created on: Jan 7, 2019
 *      Author: mstarosz
 */

#include "inet/common/INETDefs.h"
#include "ZrtpInterface.h"

#ifndef INET_APPLICATIONS_ZRTPAPP_ZRTPSENDER_H_
#define INET_APPLICATIONS_ZRTPAPP_ZRTPSENDER_H_

namespace inet {

namespace rtp {

class ZrtpSender : public ZrtpInterface {
public:
    ZrtpSender();
    virtual ~ZrtpSender();

    virtual void handleMessage(cMessage *msgIn) override;

    virtual RtpInnerPacket* prepareHello() override;

    virtual RtpInnerPacket* prepareHelloAck() override;

    virtual RtpInnerPacket* prepareDHPart1() override;

    virtual RtpInnerPacket* prepareConfirm1() override;

    virtual RtpInnerPacket* prepareConf2Ack() override;
};

}
}
#endif /* INET_APPLICATIONS_ZRTPAPP_ZRTPSENDER_H_ */

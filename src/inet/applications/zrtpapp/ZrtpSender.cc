/*
 * ZrtpSender.cc
 *
 *  Created on: Jan 7, 2019
 *      Author: mstarosz
 */

#include "ZrtpSender.h"
#include "inet/transportlayer/rtp/zrtp/ZrtpHeader_m.h"

namespace inet {

namespace rtp {

ZrtpSender::ZrtpSender() {
    // TODO Auto-generated constructor stub

}

ZrtpSender::~ZrtpSender() {
    // TODO Auto-generated destructor stub
}

void ZrtpSender::handleMessage(cMessage *msgIn) {
    ZrtpInterface::handleMessage(msgIn);

    switch (currentState) {
        case State::W8_FOR_HELLO_ACK: {
            currentState = State::W8_FOR_HELLO;
            break;
        } case State::W8_FOR_HELLO: {
            msgQueue->insert(prepareHelloAck());
            currentState = State::W8_FOR_COMMIT;
            break;
        } case State::W8_FOR_COMMIT: {
            msgQueue->insert(prepareDHPart1());
            currentState = State::W8_FOR_DH_PART_2;
            break;
        } case State::W8_FOR_DH_PART_2: {
            msgQueue->insert(prepareConfirm1());
            currentState = State::W8_FOR_CONFIRM_2;
            break;
        } case State::W8_FOR_CONFIRM_2: {
            msgQueue->insert(prepareConf2Ack());
            currentState = State::SESSION_ESTABLISHED;
            break;
        } default: {
            break;
        }
    }
}

RtpInnerPacket* ZrtpSender::prepareHello() {
    EV_INFO << "Preparing ZRTP Hello Packet" << endl;
    Packet *packet = new Packet("Hello");

    const auto& zrtpHello = makeShared<ZrtpHello>();

    srand (time(NULL));
    sequenceNumber = rand() % SHRT_MAX + 1;
    zrtpHello->setSequenceNumber(sequenceNumber);
    zrtpHello->setChunkLength(B(zrtpHello->getLength()));
    zrtpHello->setHashTypeCount(1);
    zrtpHello->setHashAlghoritmsArraySize(1);
    zrtpHello->setHashAlghoritms(0, HASH_TYPE_BLOCK::S256);
    zrtpHello->setCipherTypeCount(1);
    zrtpHello->setCipherAlghoritmsArraySize(1);
    zrtpHello->setCipherAlghoritms(0, CIPHER_TYPE_BLOCK::AES2);
    zrtpHello->setAuthTypeCount(1);
    zrtpHello->setAuthTagTypesArraySize(1);
    zrtpHello->setAuthTagTypes(0, AUTH_TAG_TYPE_BLOCK::HS32);
    zrtpHello->setKeyAgreementTypeCount(1);
    zrtpHello->setKeyAgreementTypesArraySize(1);
    zrtpHello->setKeyAgreementTypes(0, KEY_AGREEMENT_TYPE_BLOCK::DH3K);
    zrtpHello->setSasTypeCount(2);
    zrtpHello->setSasTypesArraySize(2);
    zrtpHello->setSasTypes(0, SAS_TYPE_BLOCK::B32);
    zrtpHello->setSasTypes(1, SAS_TYPE_BLOCK::B256);

    packet->insertAtFront(zrtpHello);
    RtpInnerPacket *rtpInnerPacket = new RtpInnerPacket();
    rtpInnerPacket->setDataOutPkt(packet);
    return rtpInnerPacket;
}

RtpInnerPacket* ZrtpSender::prepareHelloAck() {
    EV_INFO << "Preparing ZRTP HelloACK Packet" << endl;
    Packet *packet = new Packet("HelloACK");
    const auto& zrtpHelloACK = makeShared<ZrtpHelloAck>();

    sequenceNumber = sequenceNumber + 1;
    zrtpHelloACK->setSequenceNumber(sequenceNumber);

    zrtpHelloACK->setChunkLength(B(zrtpHelloACK->getLength()));
    packet->insertAtFront(zrtpHelloACK);
    RtpInnerPacket *rtpInnerPacket = new RtpInnerPacket();
    rtpInnerPacket->setDataOutPkt(packet);
    return rtpInnerPacket;
}

RtpInnerPacket* ZrtpSender::prepareDHPart1() {
    EV_INFO << "Preparing ZRTP DHPart1 Packet" << endl;
    Packet *packet = new Packet("DHPart1");
    const auto& zrtpDHPart1 = makeShared<ZrtpDHPart1>();

    sequenceNumber = sequenceNumber + 1;
    zrtpDHPart1->setSequenceNumber(sequenceNumber);

    zrtpDHPart1->setChunkLength(B(zrtpDHPart1->getLength()));
    packet->insertAtFront(zrtpDHPart1);
    RtpInnerPacket *rtpInnerPacket = new RtpInnerPacket();
    rtpInnerPacket->setDataOutPkt(packet);
    return rtpInnerPacket;
}

RtpInnerPacket* ZrtpSender::prepareConfirm1() {
    EV_INFO << "Preparing ZRTP Confirm1 Packet" << endl;
    Packet *packet = new Packet("Confirm1");
    const auto& zrtpConfirm1 = makeShared<ZrtpConfirm1>();

    sequenceNumber = sequenceNumber + 1;
    zrtpConfirm1->setSequenceNumber(sequenceNumber);

    zrtpConfirm1->setChunkLength(B(zrtpConfirm1->getLength()));
    packet->insertAtFront(zrtpConfirm1);
    RtpInnerPacket *rtpInnerPacket = new RtpInnerPacket();
    rtpInnerPacket->setDataOutPkt(packet);
    return rtpInnerPacket;
}

RtpInnerPacket* ZrtpSender::prepareConf2Ack() {
    EV_INFO << "Preparing ZRTP Conf2ACK Packet" << endl;
    Packet *packet = new Packet("Conf2ACK");
    const auto& zrtpConf2Ack = makeShared<ZrtpConf2Ack>();

    sequenceNumber = sequenceNumber + 1;
    zrtpConf2Ack->setSequenceNumber(sequenceNumber);

    zrtpConf2Ack->setChunkLength(B(zrtpConf2Ack->getLength()));
    packet->insertAtFront(zrtpConf2Ack);
    RtpInnerPacket *rtpInnerPacket = new RtpInnerPacket();
    rtpInnerPacket->setDataOutPkt(packet);
    return rtpInnerPacket;
}

}

}


#include "inet/transportlayer/rtp/zrtp/ZrtpHeader_m.h"

namespace inet {

namespace rtp {

void ZrtpHello::logInfo() const {
    ZrtpHeader::logInfo();
    EV_INFO << "         ZRTP protocol version: " << zrtpProtocolVersion << endl;
    EV_INFO << "         Client identifier: " << clinetIdentifier << endl;
    EV_INFO << "         Hash image: " << hashImageH3 << endl;
    EV_INFO << "         ZID: " << zid << endl;
    EV_INFO << "         Sig.capable: " << sigCapable << endl;
    EV_INFO << "         MiTM: " << mimt << endl;
    EV_INFO << "         Passive: " << passive << endl;
    EV_INFO << "         Hash type count: " << +hashTypeCount << endl;
    for (int i = 0; i < hashTypeCount; i++) {
        EV_INFO << "            Hash alghoritm[" << i << "]: " << translateHashType(hashAlghoritms[i]) << endl;
    }
    EV_INFO << "         Cipher type count: " << +cipherTypeCount << endl;
    for (int i = 0; i < cipherTypeCount; i++) {
        EV_INFO << "            Cipher alghoritm[" << i << "]: " << translateCipherAlghoritm(cipherAlghoritms[i]) << endl;
    }
    EV_INFO << "         Auth tag type count: " << +authTypeCount << endl;
    for (int i = 0; i < authTypeCount; i++) {
        EV_INFO << "            Auth tag type[" << i << "]: " << translateAuthTagType(authTagTypes[i]) << endl;
    }
    EV_INFO << "         Key agreement type count: " << +keyAgreementTypeCount << endl;
    for (int i = 0; i < keyAgreementTypeCount; i++) {
        EV_INFO << "            Key agreement type[" << i << "]: " << translateKeyAgreementType(keyAgreementTypes[i]) << endl;
    }
    EV_INFO << "         Sas type count: " << +sasTypeCount << endl;
    for (int i = 0; i < sasTypeCount; i++) {
        EV_INFO << "            Sas type[" << i << "]: " << translateSasType(sasTypes[i]) << endl;
    }
    EV_INFO << "         HMAC: " << hmac << endl;
    EV_INFO << "   Chacksum: " << chacksum << endl;

}

} // namespace rtp

} // namespace inet


#include "inet/transportlayer/rtp/zrtp/ZrtpHeader_m.h"

namespace inet {

namespace rtp {


void ZrtpHeader::logInfo() const {
    EV_INFO << "ZRTP Protocol" << endl;
    EV_INFO << "   RTP version: " << +version << endl;
    EV_INFO << "   RTP padding: " << padding << endl;
    EV_INFO << "   RTP extension: " << extension << endl;
    EV_INFO << "   Sequence: " << sequenceNumber << endl;
    EV_INFO << "   Magic cookie: " << magicCookie << endl;
    EV_INFO << "   Source identifier: " << ssrc << endl;
    EV_INFO << "      Signature: " << signature << endl;
    EV_INFO << "      Length: " << length << endl;
    EV_INFO << "      Type: " << translateType() << endl;
}

std::string ZrtpHeader::translateType() const {
    if (type == MESSAGE_TYPE_BLOCK::HELLO) {
      return "Hello";
    } else if (type == MESSAGE_TYPE_BLOCK::HELLO_ACK) {
        return "HelloACK";
    } else if (type == MESSAGE_TYPE_BLOCK::COMMIT) {
        return "Commit";
    } else if (type == MESSAGE_TYPE_BLOCK::DH_PART_1) {
        return "DHPart1";
    } else if (type == MESSAGE_TYPE_BLOCK::DH_PART_2) {
        return "DHPart2";
    } else if (type == MESSAGE_TYPE_BLOCK::CONFIRM_1) {
        return "Confirm1";
    } else if (type == MESSAGE_TYPE_BLOCK::CONFIRM_2) {
        return "Confirm2";
    } else if (type == MESSAGE_TYPE_BLOCK::CONF_2_ACK) {
        return "Conf2ACK";
    } else if (type == MESSAGE_TYPE_BLOCK::ERROR) {
        return "Error";
    } else if (type == MESSAGE_TYPE_BLOCK::ERROR_ACK) {
        return "ErrorACK";
    } else if (type == MESSAGE_TYPE_BLOCK::GO_CLEAR) {
        return "GoClear";
    } else if (type == MESSAGE_TYPE_BLOCK::CLEAR_ACK) {
        return "ClearACK";
    } else if (type == MESSAGE_TYPE_BLOCK::SAS_RELAY) {
        return "SASRelay";
    } else if (type == MESSAGE_TYPE_BLOCK::RELAY_ACK) {
        return "RelayACK";
    } else if (type == MESSAGE_TYPE_BLOCK::PING) {
        return "Ping";
    } else if (type == MESSAGE_TYPE_BLOCK::PING_ACK) {
        return "PingACK";
    }
    return "";
}

std::string ZrtpHeader::translateHashType(short hashType) const {
    if (hashType == HASH_TYPE_BLOCK::S256) {
        return "SHA-256 Hash defined in FIPS 180-3";
    } else if (hashType == HASH_TYPE_BLOCK::S384) {
        return "SHA-384 Hash defined in FIPS 180-3";
    } else if (hashType == HASH_TYPE_BLOCK::N256) {
        return "NIST SHA-3 256-bit hash";
    } else if (hashType == HASH_TYPE_BLOCK::N384) {
        return "NIST SHA-3 384-bit hash";
    } else {
        return "";
    }
}

std::string ZrtpHeader::translateCipherAlghoritm(short cipherAlghoritm) const {
    if (cipherAlghoritm == CIPHER_TYPE_BLOCK::AES1) {
        return "AES with 128-bit keys";
    } else if (cipherAlghoritm == CIPHER_TYPE_BLOCK::AES2) {
        return "AES with 192-bit keys";
    } else if (cipherAlghoritm == CIPHER_TYPE_BLOCK::AES3) {
        return "AES with 256-bit keys";
    } else if (cipherAlghoritm == CIPHER_TYPE_BLOCK::FS1) {
        return "TwoFish with 128-bit keys";
    } else if (cipherAlghoritm == CIPHER_TYPE_BLOCK::FS2) {
        return "TwoFish with 192-bit keys";
    } else if (cipherAlghoritm == CIPHER_TYPE_BLOCK::FS3) {
        return "TwoFish with 256-bit keys";
    } else {
        return "";
    }
}

std::string ZrtpHeader::translateAuthTagType(short authTagType) const {
    if (authTagType = AUTH_TAG_TYPE_BLOCK::HS32) {
        return "32-bit authentication tag based on HMAC-SHA1 as defined in RFC 3711";
    } else if (authTagType == AUTH_TAG_TYPE_BLOCK::HS80) {
        return "80-bit authentication tag based on HMAC-SHA1 as defined in RFC 3711";
    } else if (authTagType == AUTH_TAG_TYPE_BLOCK::SK32) {
        return "32-bit authentication tag based on Skein-512-MAC as defined in [Skein], with 256-bit key, 32-bit MAC lenght";
    } else if (authTagType == AUTH_TAG_TYPE_BLOCK::SK64) {
        return "64-bit authentication tag based on Skein-512-MAC as defined in [Skein], with 256-bit key, 64-bit MAC lenght";
    } else {
        return "";
    }
}

std::string ZrtpHeader::translateKeyAgreementType(short keyAgreementType) const {
    if (keyAgreementType == KEY_AGREEMENT_TYPE_BLOCK::DH2K) {
        return "pv words 64, message words 85, DH mode with p=2048 bit prime per RFC 3525, Section 3";
    } else if (keyAgreementType == KEY_AGREEMENT_TYPE_BLOCK::DH3K) {
        return "pv words 96, message words 117, DH mode with p=3072 bit prime per RFC 3525, Section 4";
    } else if (keyAgreementType == KEY_AGREEMENT_TYPE_BLOCK::EC25) {
        return "pv words 16, message words 37, Elliptic Curve DH, P-256 per RFC 5114, Section 2.6";
    } else if (keyAgreementType == KEY_AGREEMENT_TYPE_BLOCK::EC38) {
        return "pv words 24, message words 45, Elliptic Curve DH, P-384 per RFC 5114, Section 2.7";
    } else if (keyAgreementType == KEY_AGREEMENT_TYPE_BLOCK::EC52) {
        return "pv words 33, message words 54, Elliptic Curve DH, P-521 per RFC 5114, Section 2.8 (deprecated - do not use)";
    } else if (keyAgreementType == KEY_AGREEMENT_TYPE_BLOCK::MULT) {
        return "Preshared Non-DH mode";
    } else if (keyAgreementType == KEY_AGREEMENT_TYPE_BLOCK::PRSH) {
        return "Multistream Non-DH mode";
    } else {
        return "";
    }
}

std::string ZrtpHeader::translateSasType(short sasType) const {
    if (sasType == SAS_TYPE_BLOCK::B32) {
        return "Short Authentication String using base32 encoding";
    } else if (sasType == SAS_TYPE_BLOCK::B256) {
        return "Short Authentication String using base256 encoding (PGP Word List)";
    } else {
        return "";
    }
}


} // namespace rtp

} // namespace inet


#include "inet/transportlayer/rtp/zrtp/ZrtpHeader_m.h"

namespace inet {

namespace rtp {

void ZrtpConfirm1::logInfo() const {
    ZrtpHeader::logInfo();
    EV_INFO << "   Chacksum: " << chacksum << endl;
}


} // namespace rtp

} // namespace inet

